﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Microsoft.Office.Interop.Excel;
using System.IO;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Data.Linq.Mapping;
using System.Data.Linq;
using System.Data;

namespace DB2XLS
{
    class Program
    {
        static void Main(string[] args)
        {
            string databaseName = Properties.Settings.Default.DatabaseCatalogName;
            Console.WriteLine("Έτος Βάσης Δεδομένων: ");
            string input = Console.ReadLine();
            if(input != "")
            {
                input = "Farnet_" + input;
                databaseName = input;
                Properties.Settings.Default.DatabaseCatalogName = input;
                Properties.Settings.Default.Save();
            }
            if (databaseName == "")
                databaseName = "Farnet_2013";

            string directory = @"C:\pfsyn\", filenamePelates = @"pelates", suffix = @".xlsx";

            string dataSourceString = @"Data Source=(local)\CSASQL;Initial Catalog=" + databaseName + ";Integrated Security=True";

            string getCustomersQuery = "SELECT PE_CODE, PE_EPWNYMIA, PE_ONOMA, PE_AMKA, PE_CODE FROM Farnet_2013.dbo.PELATES ORDER BY PE_EPWNYMIA;";

            int maxRows;

            FileInfo newFile = new FileInfo(directory + filenamePelates + suffix);

            try { Directory.CreateDirectory(directory); }
            catch (Exception e)
            {
                if (e is DirectoryNotFoundException || e is UnauthorizedAccessException)
                    Console.WriteLine("Λείπει ο φάκελος: " + directory);
                else
                    Console.WriteLine("Το όνομα φακέλου: " + directory + " δεν είναι έγκυρο.");
                Console.WriteLine("Τα αρχεία θα δημιουργηθούν στο φάκελο του προγράμματος.\nΓια να μην επαναληφθεί αυτό το μήνυμα, δημιουργήστε το φάκελο " + directory);
                SystemPause();

                directory = "";
            }
            
            if (newFile.Exists)
            {
                newFile.Delete();
                /*try
                {
                    for (int i = 1; ; i++)
                    {
                        newFile = new FileInfo(directory + filenamePelates + "(" + i + ")" + suffix);
                        if (newFile.Exists == false)
                            break;
                        if (i > 5000)
                        {
                            Console.WriteLine("Έχετε υπερβολικά πολλά αντίγραφα, καθαρίστε το: " + directory);
                            SystemPause();
                            return;
                        }
                    }
                }
                catch (System.AccessViolationException)
                {
                    Console.WriteLine("Δεν έχετε δικαίωμα εγραφής στο αρχείο ή το αρχείο είναι ήδη ανοιχτό");
                    SystemPause();
                    return;
                }*/
            }
            using (ExcelPackage package = new ExcelPackage(newFile))
            {
                // add a new worksheet to the empty workbook
                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("Πελάτες");
                //Add the headers
                worksheet.Cells[1, 1].Value = "code";
                worksheet.Cells[1, 2].Value = "surname";
                worksheet.Cells[1, 3].Value = "name";
                worksheet.Cells[1, 4].Value = "amka";
                worksheet.Cells[1, 5].Value = "code_1";

                using (SqlConnection conn = new SqlConnection())
                using (SqlCommand cmd = new SqlCommand(getCustomersQuery, conn))
                {
                    conn.ConnectionString = dataSourceString;
                    try { conn.Open(); }
                    catch(Exception e)
                    {
                        Console.WriteLine("Δεν βρέθηκε η Βάση Δεδομένων, φορτώνεται η Farnet_2013");
                        dataSourceString = @"Data Source=(local)\CSASQL;Initial Catalog=Farnet_2013;Integrated Security=True";
                        conn.ConnectionString = dataSourceString;
                        conn.Open();
                    }
                    /*System.Data.DataTable databases = conn.GetSchema("Databases");
                    foreach (System.Data.DataRow database in databases.Rows)
                    {
                        String currName = database.Field<String>("database_name");
                        DateTime creationDate = database.Field<DateTime>("create_date");
                        Console.WriteLine(currName + "   " + creationDate.ToString());
                    }*/
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        int j = 1;
                        while (reader.Read())
                        {
                            j++;
                            for (int i = 0; i < reader.FieldCount; i++)
                            {

                                worksheet.Cells[j, i + 1].Value = reader.GetValue(i);
                                if (i == 4)
                                    worksheet.Cells[j, i + 1].Style.Numberformat.Format = "000000";
                                else
                                    worksheet.Cells[j, i + 1].Style.Numberformat.Format = "@";

                            }
                        }
                        maxRows = j;
                    }

                    //worksheet.Cells["D2:E5"].Style.Numberformat.Format = "#,##0.00";

                    //worksheet.Cells["A2:A4"].Style.Numberformat.Format = "@";   //Format as text

                    worksheet.Cells.AutoFitColumns(0);  //Autofit columns for all cells

                    // lets set the header text 
                    worksheet.HeaderFooter.OddHeader.CenteredText = "&24&U&\"Arial,Regular Bold\" Πελατολόγιο";
                    // add the page number to the footer plus the total number of pages

                    package.Workbook.Properties.Title = "Πελατολόγιο";
                    package.Workbook.Properties.Author = "Κ. Γκίνης";

                    package.Workbook.Properties.Company = "Φαρμακείο Π. Γκίνη";

                    package.Save();
                }
            }

            /*string DBFilepath = @"C:\FarmakoNet\SQLData\Farnet_2013.MDF";
            string DBConnectionString = @"Data Source=(local)\SQLEXPRESS;Initial Catalog=Farnet_2013;Integrated Security=True";
            DataContext db = new DataContext(DBConnectionString);

            // Get a typed table to run queries.
            Table<Customer> Customers = db.GetTable<Customer>();

            db.Log = Console.Out;

            // Query for customers in London.
            IQueryable<Customer> custQuery =
                from cust in Customers
                where cust.PE_ONOMAD == "ΠΕΤΡΟΣ"
                select cust;
            foreach (Customer cust in custQuery)
            {
                Console.WriteLine("ID={0}, City={1}", cust.PE_ID, cust.PE_ONOMAD);
            }
            Console.WriteLine(custQuery.Count()+ " Customers");
            // Prevent console window from closing.
            Console.ReadKey();*/
        }

        private static void SystemPause()
        {
            Console.WriteLine("\r\nΠιέστε ένα πλήκτρο για κλείσιμο");
            Console.ReadKey();
        }
    }
}

[Table(Name = "PELATES")]
public class Customer
{
    private int _PE_IDD;
    [Column(IsPrimaryKey = true, Storage = "_PE_IDD")]
    //public int PE_ID { get; set; }
    public int PE_ID { get { return this._PE_IDD; } set { this._PE_IDD = value; } }
    private string _PE_ONOMAD;
    [Column(Storage = "_PE_ONOMAD")]
    public string PE_ONOMAD { get { return this._PE_ONOMAD; } set { this._PE_ONOMAD = value; } }
}